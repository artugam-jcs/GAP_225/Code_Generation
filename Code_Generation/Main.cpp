#include <iostream>

#define PRIVATE_VARIABLE_WITH_PUBLIC_GETTER_AND_SETTER(type, name) \
private: \
    type m_##name##; \
public: \
    void Set##name##(type val) { this->m_##name## = val; } \
    type Get##name##() { return this->m_##name##; }

class Foo
{
    PRIVATE_VARIABLE_WITH_PUBLIC_GETTER_AND_SETTER(int, foo)
};

int main()
{
    Foo foo;

    foo.Setfoo(10);

    int val = foo.Getfoo();

    std::cout << val;

	return 0;
}
